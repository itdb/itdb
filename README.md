

## About ITDB

ITDB is a web application for IT documentation - it enabled IT service providers, MSPs & IT administrators to have a central place for documentation. No more spreadsheets and textfiles all over the place - document your hardware, networks and services cleanly structured in central place.

## Planned Features

* Network documentation (interfaces, networks, prefixes, wiring,...)
* Hardware documentation (switches, servers, routers,...)
* Service documentation (web services, mail services,...)
* Multi-organization support (assign entities to specific organisations)
* Fine-grained access control (restrict users from viewing/editing/creating entities based on organisation, entity type or security level)
* Change management - plan, track and apply changes - allow peer-reviews of changes before applying them

## Contributing

Contributions are always welcome! Please consider opening an issue in [Gitlab](https://gitlab.com/itdb/itdb/-/issues)


## License

ITDB is open-sourced under the [GPLv3 license](http://www.gnu.org/licenses/gpl-3.0.html). 
