/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import vSelect from 'vue-select'
import VueLodash from 'vue-lodash'
import get from 'lodash/get'


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
/*
const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
*/

Vue.component('v-select', vSelect)

/* Components */

Vue.component('vue-form', require('./components/Form').default);
Vue.component('form-group', require('./components/FormGroup.vue').default);
Vue.component('select-input', require('./components/SelectInput.vue').default);
Vue.component('table-row', require('./components/TableRow.vue').default);
Vue.component('loading', require('./components/Loading.vue').default);
Vue.component('tabs', require('./components/Tabs.vue').default);
Vue.component('change-indicator', require('./components/ChangeIndicator.vue').default);
Vue.component('change-indicator-notification', require('./components/ChangeIndicatorNotification.vue').default);


/* Create Forms */
Vue.component('create-change-form', require('./components/CreateForms/CreateChangeForm.vue').default);

/* Views */
Vue.component('organization-view', require('./components/Views/OrganizationView.vue').default);
Vue.component('change-view', require('./components/Views/ChangeView.vue').default);


/* Tables */
Vue.component('service-table', require('./components/Tables/ServiceTable.vue').default);
Vue.component('service-type-table', require('./components/Tables/ServiceTypeTable.vue').default);
Vue.component('organization-table', require('./components/Tables/OrganizationTable.vue').default);
Vue.component('change-table', require('./components/Tables/ChangeTable.vue').default);

/* Filters */
Vue.component('service-filter-form', require('./components/FilterForms/ServiceFilterForm').default);
Vue.component('organization-filter-form', require('./components/FilterForms/OrganizationFilterForm').default);
Vue.component('change-filter-form', require('./components/FilterForms/OrganizationFilterForm').default);

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.use(VueLodash, { name: 'custom', lodash: { get } })

const store = new Vuex.Store({
    state: {
        user: []
    },
    mutations: {
        updateUser (state, user) {
            state.user = user;
        }
    },
    getters: {
        getUser: (state) => {
            return state.user;
        }
    }
})

var router = new VueRouter({
    mode: 'history',
    routes: []
});

var _get = require("lodash/core");


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    router,
    store,
    el: '#app',
    data: {
        sidebar_collapsed: false,
    },
    methods: {
        update_user: function() {
            axios.get('/api/user').then(response => {
                store.commit("updateUser",response.data)
                console.log(this.$store.getters.getUser);
            }).catch(error => {
                console.error("Error fetching user data.",error)
            });
        },
        collapse_sidebar: function(event) {
            this.sidebar_collapsed = !this.sidebar_collapsed;
        },
        refreshCSRFCookie: function (callback = null) {
            axios.get('/sanctum/csrf-cookie').then(response => {
                if(callback)
                    callback(response);
                this.update_user();
            }).catch(error => {
                console.error("Error fetching CSRF cookie.")
            });
        }
    },
    mounted() {
        if(this.$route.path !== "/login")
            this.refreshCSRFCookie();
    }
});
