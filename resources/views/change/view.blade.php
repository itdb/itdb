@extends("layouts.app")

@section("content")
    <change-view :change="{{ json_encode($change) }}"></change-view>
@endsection
