@extends("layouts.app")

@section("header")
    <h1>Changes</h1>
    <a href="change/new" class="btn btn-success"><b-icon-plus></b-icon-plus>Add Change</a>
@endsection

@section("content")
    <change-table v-bind:items="{{ json_encode($changes->toArray()['data'])}}"></change-table>
@endsection

@section("sidebar")
    <h2>Search</h2>

    <change-filter-form></change-filter-form>
@endsection
