@extends("layouts.app")

@section("content")
    <h1>Create change</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{ json_encode(old()) }}
    {{ json_encode($errors->toArray()) }}
    <create-change-form :errors="{{ json_encode($errors->toArray()) }}" :old="{{ json_encode(old()) }}"></create-change-form>
@endsection
