@extends("layouts.app")


@section("content")
    <h1>Service Type</h1>
    <a href="service/type/add" class="btn btn-success mb-2"><b-icon-plus></b-icon-plus>Add Service Type</a>
    <service-type-table v-bind:items="{{ json_encode($service_types->toArray()['data']) }}"></service-type-table>
@endsection

@section("sidebar")
    <a href="service/type/add" class="btn btn-success mb-2"><b-icon-plus></b-icon-plus>Add Service Type</a>
@endsection
