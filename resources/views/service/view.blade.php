@extends("layouts.app")

@section("content")
    {{ json_encode($service) }}
    <br>
    {{json_encode($service->current_service_version)}}
    <br>
    {{json_encode($service->getRecentVersions())}}
@endsection
