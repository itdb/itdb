@extends("layouts.app")

@section("header")
    <h1>Service</h1>
    <a href="service/add" class="btn btn-success"><b-icon-plus></b-icon-plus>Add Service</a>
@endsection

@section("content")
    <service-table v-bind:items="{{ json_encode($services->toArray()['data']) }}"></service-table>
    {{ $services->withQueryString()->links() }}
@endsection

@section("sidebar")
    <h2>Search</h2>

    <service-filter-form></service-filter-form>
@endsection
