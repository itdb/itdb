@extends("layouts.app")

@section("header")
    <h1>Organization</h1>
    <a href="service/add" class="btn btn-success"><b-icon-plus></b-icon-plus>Add Organization</a>
@endsection

@section("content")
    <organization-table v-bind:items="{{ json_encode($organizations->toArray()['data'])}}"></organization-table>
@endsection

@section("sidebar")
    <h2>Search</h2>

    <organization-filter-form></organization-filter-form>
@endsection
