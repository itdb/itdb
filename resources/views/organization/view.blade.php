@extends("layouts.app")

@section("content")
    <organization-view :organization="{{ json_encode($organization) }}"></organization-view>
@endsection
