@extends('layouts.main')

@section("app")
<div id="app" v-bind:class="{ 'sidebar-collapsed' : sidebar_collapsed}">
    <nav class="nb-navbar">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                <li class="nav-item">
                    <a class="nb-nav-link">
                        <change-indicator></change-indicator>
                    </a>
                </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nb-nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
            </ul>
        </div>
    </nav>
    <div class="nb-container">
        <div class="row">
            <transition name="fade">
            <nav class="nb-sidebar">
                <div class="nb-sidebar-menu">
                    <ul class="nav flex-column">
                        <li class="nav-item {{ (request()->is('dashboard>*')) ? 'active' : '' }}">
                            <a class="nb-nav-link" href="/dashboard">
                                <b-icon-house class="nb-nav-link-icon"></b-icon-house>
                                <span class="nb-nav-link-text">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item {{ (request()->is('network*')) ? 'active' : '' }}">
                            <a class="nb-nav-link" href="/network">
                                <b-icon-hdd-network class="nb-nav-link-icon"></b-icon-hdd-network>
                                <span class="nb-nav-link-text">Network</span>
                            </a>
                            <ul class="nb-sidebar-sub-menu">
                                <li class="nav-item {{ (request()->is('network')) ? 'active' : '' }}">
                                    <a class="nb-nav-link" href="/network">
                                        <span class="nb-nav-link-text">List</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item{{ (request()->is('Hardware')) ? 'active' : '' }}">
                            <a class="nb-nav-link" href="/hardware">
                                <b-icon-hdd-rack class="nb-nav-link-icon"></b-icon-hdd-rack>
                                <span class="nb-nav-link-text">Hardware</span>
                            </a>
                        </li>
                        <li class="nav-item {{ (request()->is('service*')) ? 'active' : '' }}">
                            <a class="nb-nav-link" href="{{ route("service") }}">
                                <b-icon-gear class="nb-nav-link-icon"></b-icon-gear>
                                <span class="nb-nav-link-text">Service</span>
                            </a>
                            <ul class="nb-sidebar-sub-menu">
                                <li class="nav-item {{ (request()->is('service')) ? 'active' : '' }}">
                                    <a class="nb-nav-link" href="{{ route("service") }}">
                                        <span class="nb-nav-link-text">List</span>
                                    </a>
                                </li>
                                <li class="nav-item {{ (request()->is('service/type')) ? 'active' : '' }}">
                                    <a class="nb-nav-link" href="{{ route("service.types") }}">
                                        <span class="nb-nav-link-text">Types</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item {{ (request()->is('change*')) ? 'active' : '' }}">
                            <a class="nb-nav-link" href="/change">
                                <b-icon-clipboard class="nb-nav-link-icon"></b-icon-clipboard>
                                <span class="nb-nav-link-text">Change</span>
                            </a>
                        </li>

                        <li class="nav-item {{ (request()->is('organization*')) ? 'active' : '' }}">
                            <a class="nb-nav-link" href="/organization">
                                <b-icon-building class="nb-nav-link-icon"></b-icon-building>
                                <span class="nb-nav-link-text">Organization</span>
                            </a>
                        </li>
                    </ul>

                    <ul class="nb-sidebar-menu-bottom">
                        <li class="nav-item">
                            <a v-on:click="collapse_sidebar" class="nb-nav-link nb-nav-link-collapse" href="#">
                                <b-icon-chevron-double-left class="nb-nav-link-icon-collapse nb-nav-link-icon"></b-icon-chevron-double-left>
                                <b-icon-chevron-double-right class="nb-nav-link-icon-expand nb-nav-link-icon"></b-icon-chevron-double-right>
                                <span class="nb-nav-link-text">Collapse </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            </transition>
            <main class="nb-main">
                <change-indicator-notification></change-indicator-notification>
                @hasSection("header")
                <div class="nb-main-header">
                    @yield("header")
                </div>
                @endif

                <div class="nb-main-inner">
                    <div class="nb-main-content">
                    @yield('content')
                    </div>
                    @hasSection("sidebar")
                        <aside class="nb-main-sidebar">
                            @yield("sidebar")
                        </aside>
                    @endif
                </div>
            </main>

        </div>
    </div>
</div>
@endsection
