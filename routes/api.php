<?php

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Authenticated API



Route::middleware('auth:sanctum')->group(function() {
  Route::get('user', function (Request $request) {
      return User::with("activeChange")->findOrFail(Auth::id());
  });
    Route::get("organizations", function (Request $request, \App\Services\OrganizationService $organizationService) {
        return Response::json($organizationService->getAllOrganizations($request->all())->get());
    });
    Route::prefix("service")->group(function() {
        Route::get("/", "Service\ServiceControllerAPI@index")->name("api.service");
    });

    Route::prefix("change")->group(function() {
        Route::get("reason", function () {
            return Response::json(\App\Models\ChangeReason::all());
        })->name("api.change.reason");
    });
});



