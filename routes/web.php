<?php

use App\Http\Controllers\Change\ChangeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("/dashboard");
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get("/dashboard", "Dashboard\DashboardController@index")->name("dashboard");

Route::prefix("service")->group(function() {
    Route::get("/", "Service\ServiceController@index")->name("service");

    Route::get("/type", "Service\ServiceTypeController@index")->name("service.types");

    Route::get("/{id}","Service\ServiceController@view")->name("service.view")->where("id","[0-9]*");
});

Route::prefix("organization")->group(function() {
    Route::get("/", "Organization\OrganizationController@index")->name("organization");
    Route::get("/{id}","Organization\OrganizationController@view")->name("organization.view")->where("id","[0-9]*");
});

Route::prefix("user")->group(function() {
    Route::post("/activeChange",[\App\Http\Controllers\User\UserController::class, 'activeChange'])->name("user.activateChange");
});

Route::prefix("change")->group(function() {
    Route::get("/", "Change\ChangeController@index")->name("change");
    Route::post("/",[ChangeController::class, 'store'])->name("change.store");
    Route::get("/new",[ChangeController::class, 'create'])->name("change.new");
    Route::get("/{id}","Change\ChangeController@view")->name("change.view")->where("id","[0-9]*");
});
