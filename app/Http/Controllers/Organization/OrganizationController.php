<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\RequestInputFilter;
use App\Organization;
use App\Repositories\FilterableRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\RepositoryFilter;
use App\Repositories\ServiceRepository;
use App\RequestRepositoryFilter;
use App\Services\OrganizationService;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    //use RequestRepositoryFilter;

    protected $filter_properties = ["name"];

    protected OrganizationService $organizationService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(OrganizationService $organizationService)
    {
        $this->middleware('auth');
        $this->organizationService = $organizationService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return view('organization.index')->with("organizations",$this->organizationService->getAllOrganizations($request->all($this->filter_properties))->simplePaginate(50));
    }

    public function view(Request $request) {
        return view('organization.view')->with(['organization'=>$this->organizationService->getOrganization($request->route("id"))]);
    }
}
