<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\ChangeService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected UserService $userService;
    protected ChangeService $changeSevice;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $userService, ChangeService $changeService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
        $this->changeSevice = $changeService;
    }

    public function activeChange(Request $request) {
        $this->userService->makeChangeActiveForUser($request->input("change_id"),Auth::user());

        return "OK";
    }

}
