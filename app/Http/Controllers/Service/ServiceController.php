<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Repositories\RepositoryFilter;
use App\Repositories\ServiceRepository;
use App\Services\ServiceService;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    protected $filter_properties = ["name","description","type","organization_id","organization_name"];

    protected $serviceService;

    /**
     * Create a new controller instance.
     *
     * @param ServiceService $serviceService
     */
    public function __construct(ServiceService $serviceService) {
        $this->middleware('auth');
        $this->serviceService = $serviceService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) {
        return view('service.index')->with("services",$this->serviceService->getAllServices($request->all($this->filter_properties))->paginate(1));
    }

    public function view(Request $request) {
        $service = $this->serviceService->getService($request->route("id"));

        return view('service.view')->with(['service'=>$service]);
    }
}
