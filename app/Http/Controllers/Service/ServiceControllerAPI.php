<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Repositories\RepositoryFilter;
use App\Repositories\ServiceRepository;
use App\Services\ServiceService;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class ServiceControllerAPI extends Controller
{
    protected $filter_properties = ["name","description","type","organization_id","organization_name"];
    protected $serviceService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ServiceService $serviceService)
    {
        $this->middleware('api');
        $this->serviceService = $serviceService;
    }

    /**
     * Show the application dashboard.
     *
     */
    public function index(Request $request)
    {
        $services = $this->serviceService->getAllServices($request->all($this->filter_properties))->get()->toJson();

        return $services;
    }
}
