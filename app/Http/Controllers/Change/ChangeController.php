<?php

namespace App\Http\Controllers\Change;

use App\Http\Controllers\Controller;
use App\Http\RequestInputFilter;
use App\Http\Requests\NewChangeRequest;
use App\Models\Change;
use App\Organization;
use App\Repositories\FilterableRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\RepositoryFilter;
use App\Repositories\ServiceRepository;
use App\RequestRepositoryFilter;
use App\Services\ChangeService;
use App\Services\OrganizationService;
use Illuminate\Http\Request;

class ChangeController extends Controller
{
    //use RequestRepositoryFilter;

    protected $filter_properties = ["name"];

    protected ChangeService $changeService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ChangeService $changeService)
    {
        $this->middleware('auth');
        $this->changeService = $changeService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return view('change.index')->with("changes",$this->changeService->getAllChanges($request->all($this->filter_properties))->simplePaginate(50));
    }

    public function view(Request $request) {
        return view('change.view')->with(['change'=>$this->changeService->getChange($request->route("id"))]);
    }

    public function create(Request $request) {
        return view('change.new');
    }

    public function store(NewChangeRequest $request) {
        $change = $this->changeService->create($request->all(),$request->user());

        return redirect()->route("change.view",["id" => $change->id]);
    }
}
