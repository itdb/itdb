<?php

namespace App\Repositories;

use App\Models\Organization;
use App\Models\Services;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * App\Repositories\ServiceRepository
 *
 */
class OrganizationRepository
{
    public function withAddress($filters = []) {
        return Organization::with("address")->filter($filters);
    }

    public function all($filters = []) {
        return Organization::filter($filters);
    }

    public function find(int $id) {;
        return $this->withAddress()->findOrFail($id);
    }

}
