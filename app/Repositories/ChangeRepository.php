<?php

namespace App\Repositories;

use App\Models\Change;
use App\Models\Organization;
use App\Models\Services;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * App\Repositories\ServiceRepository
 *
 */
class ChangeRepository
{
    public function withStatus($filters = []) {
        return Change::with("status")->filter($filters);
    }

    public function all($filters = []) {
        return Change::filter($filters);
    }

    public function find(int $id) {;
        return Change::findOrFail($id);
    }
}
