<?php

namespace App\Repositories;

use App\Models\Services;
use App\Models\ServiceType;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * App\Repositories\ServiceRepository
 *
 */
class ServiceTypeRepository
{
    public function all($filters = []) {
        return ServiceType::filter($filters);
    }

    public function find(int $id) {
        return ServiceType::findOrFail($id);
    }
}
