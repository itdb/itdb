<?php

namespace App\Repositories;

use App\Models\Service;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * App\Repositories\ServiceRepository
 *
 */
class ServiceRepository
{
    public function all($filters = []) {
        return Service::with(["current_service_version","service_type","organization"])->filter($filters);
    }

    public function find(int $id) {
        return Service::findOrFail($id);
    }
}
