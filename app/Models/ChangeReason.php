<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ChangeStatus
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeStatus whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeStatus whereTitle($value)
 * @mixin \Eloquent
 */
class ChangeReason extends Model
{
    use HasFactory;

    protected $table = "change_reason";

}
