<?php

namespace App\Models;

use App\Repositories\FilterableModel;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Service
 *
 * @property int $id
 * @property int $version_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereVersionId($value)
 * @mixin \Eloquent
 * @property int $service_type_id
 * @property int $current_version_id
 * @property int|null $proposed_version_id
 * @property int|null $organization_id
 * @property-read \App\Models\ServiceVersion|null $current_service_version
 * @property-read \App\Models\Organization|null $organization
 * @property-read \App\Models\ServiceVersion|null $propsed_service_version
 * @property-read \App\Models\ServiceType|null $service_type
 * @method static \Illuminate\Database\Eloquent\Builder|Service filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Service paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Service simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereCurrentVersionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereOrganizationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereProposedVersionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereServiceTypeId($value)
 */
class Service extends Model
{
    use Filterable;

    protected $table = "service";

    public function service_type() {
        return $this->hasOne("App\Models\ServiceType","id","service_type_id");
    }

    public function current_service_version() {
        return $this->hasOne("App\Models\ServiceVersion","id","current_version_id")->whereNull("superseded_by");
    }

    public function propsed_service_version() {
        return $this->hasOne("App\Models\ServiceVersion","id","proposed_version_id")->whereNull("superseded_by");
    }

    public function organization() {
        return $this->hasOne("App\Models\Organization", "id","organization_id");
    }

    public function getRecentVersions() {
        $previous_version = $this->current_service_version->previous_version;
        $recents = [];

        if(!$previous_version)
            return $recents;

        while ($previous_version->previous_version != null) {
            $recents[] = $previous_version;
            $previous_version = $previous_version->previous_version;
        }

        return $recents;
    }
}
