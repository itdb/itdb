<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Change
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $author_id
 * @property int $status_id
 * @property-read \App\Models\User|null $author
 * @property-read \App\Models\ChangeStatus|null $status
 * @method static \Illuminate\Database\Eloquent\Builder|Change newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Change newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Change query()
 * @method static \Illuminate\Database\Eloquent\Builder|Change whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Change whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Change whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Change whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Change whereTitle($value)
 * @mixin \Eloquent
 */
class Change extends Model
{
    use HasFactory, Filterable;

    protected $table = "change";

    protected $fillable = ["name","description"];

    public $timestamps = false;

    public function author() {
        return $this->hasOne("App\Models\User","author_id");
    }

    public function status() {
        return $this->hasOne("App\Models\ChangeStatus","id","status_id");
    }
}
