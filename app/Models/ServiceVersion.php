<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ServiceVersion
 *
 * @property int $id
 * @property string $name
 * @property int $superseeds
 * @property int $superseeded_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceVersion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceVersion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceVersion query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceVersion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceVersion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceVersion whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceVersion whereSuperseededBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceVersion whereSuperseeds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceVersion whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $description
 * @property int $author_id
 * @property int|null $supersedes
 * @property int|null $superseded_by
 * @property string|null $commit_hash
 * @property-read ServiceVersion|null $next_version
 * @property-read ServiceVersion|null $previous_version
 * @property-read \App\Models\Service $service
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceVersion whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceVersion whereCommitHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceVersion whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceVersion whereSupersededBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceVersion whereSupersedes($value)
 */
class ServiceVersion extends Model
{
    protected $table = "service_version";

    public function service() {
        return $this->belongsTo(Service::class);
    }

    public function user() {
        return $this->hasOne("App\Models\User","id","author_id");
    }

    public function previous_version() {
        return $this->hasOne("App\Models\ServiceVersion","id","supersedes");
    }

    public function next_version() {
        return $this->hasOne("App\Models\ServiceVersion","id","superseded_by");
    }
}
