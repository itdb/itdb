<?php


namespace App\Services;


use App\Repositories\ServiceRepository;
use App\Repositories\ServiceTypeRepository;

class ServiceService {
    protected ServiceRepository $serviceRepository;
    protected ServiceTypeRepository $serviceTypeRepository;

    public function __construct(ServiceRepository $serviceRepository, ServiceTypeRepository $serviceTypeRepository) {
        $this->serviceRepository = $serviceRepository;
        $this->serviceTypeRepository = $serviceTypeRepository;
    }

    public function getAllServices($filters = null) {
        $services = $this->serviceRepository->all($filters);

        return $services;
    }

    public function getService($id) {
        return $service = $this->serviceRepository->find($id);
    }

    public function getAllServiceTypes($filters = null) {
        return $this->serviceTypeRepository->all($filters);
    }
}
