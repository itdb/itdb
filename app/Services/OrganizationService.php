<?php


namespace App\Services;


use App\Repositories\OrganizationRepository;
use App\Repositories\RepositoryFilter;
use App\Repositories\ServiceRepository;
use App\Repositories\ServiceTypeRepository;

class OrganizationService {
    protected OrganizationRepository $organizationRepository;

    public function __construct(OrganizationRepository $organizationRepository) {
        $this->organizationRepository = $organizationRepository;
    }

    public function getAllOrganizations($repositoryFilters = []) {
        $organizations = $this->organizationRepository->withAddress($repositoryFilters);

        return $organizations;
    }

    public function getOrganization($id) {
        return $this->organizationRepository->find($id);
    }
}
