<?php


namespace App\Services;


use App\Models\Change;
use App\Models\User;
use App\Repositories\ChangeRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\ServiceTypeRepository;

class UserService {
    protected ChangeRepository $changeRepository;


    public function __construct(ChangeRepository $changeRepository) {
        $this->changeRepository = $changeRepository;
    }

    public function makeChangeActiveForUser($change_id,User $user) {
        if($change_id)
            return $user->activeChange()->associate($this->changeRepository->find($change_id))->save();
        else
            return $user->activeChange()->dissociate()->save();
    }
}
