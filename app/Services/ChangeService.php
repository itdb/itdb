<?php


namespace App\Services;


use App\Models\Change;
use App\Models\User;
use App\Repositories\ChangeRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\RepositoryFilter;
use App\Repositories\ServiceRepository;
use App\Repositories\ServiceTypeRepository;

class ChangeService {
    protected ChangeRepository $changeRepository;

    public function __construct(ChangeRepository $changeRepository) {
        $this->changeRepository = $changeRepository;
    }

    public function getAllChanges($repositoryFilters = []) {
        $changes = $this->changeRepository->withStatus($repositoryFilters);

        return $changes;
    }

    public function getChange($id) {
        return $this->changeRepository->find($id);
    }

    public function create(array $fields,User $user) {
        $change = new Change();

        $change->title = $fields["title"];
        $change->description = $fields["description"];
        $change->reason_id = $fields["reason_id"];
        $change->status_id = 1;
        $change->author_id = $user->id;

        $change->save();

        return $change;
    }
}
