<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class ServiceFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function name($name) {
        return $this->related("current_service_version","name","LIKE","%".$name."%");
    }

    public function description($description) {
        return $this->related("current_service_version","description","LIKE","%".$description."%");
    }

    public function organizationName($names) {
        if(!is_array($names)) {
            return $this->related("organization","name","LIKE",$names);
        }
        return $this->whereHas('organization', function($query) use ($names)
        {
            foreach ($names as $name)
                $query->orWhere("name", "LIKE", "%" . $name . "%");
        });
        //return $this->related("organization","name","LIKE","%".$name."%");
    }
}
