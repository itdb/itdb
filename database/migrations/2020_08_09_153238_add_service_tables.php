<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServiceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("address", function (Blueprint $table) {
            $table->id();
            $table->string("country");
            $table->string("state");
            $table->string("district");
            $table->string("street");
            $table->text("comment");
        });

        Schema::create("organization", function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->unsignedBigInteger("address_id")->nullable();
        });

        Schema::table("organization", function (Blueprint $table) {
            $table->foreign("address_id")->references("id")->on("address");
        });

        Schema::create("change_status", function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->text("description");
        });

        DB::table("change_status")->insert([
            "id" => 0,
            "title" => "open",
            "description" => "Not started yet"
        ]);

        DB::table("change_status")->insert([
            "id" => 1,
            "title" => "planned",
            "description" => "All commits are prepared but not commited"
        ]);

        DB::table("change_status")->insert([
            "id" => 2,
            "title" => "in_progress",
            "description" => "Currently in progress - some commits are not yet commited"
        ]);

        DB::table("change_status")->insert([
            "id" => 3,
            "title" => "closed",
            "description" => "aborted - commited work is still applied"
        ]);

        DB::table("change_status")->insert([
            "id" => 4,
            "title" => "completed",
            "description" => "All commits are applied - the change is finished"
        ]);

        Schema::create("change_reason", function (Blueprint $table) {
            $table->id();
            $table->string("reason");
        });

        DB::table("change_reason")->insert([
            "reason" => "Improvement",
        ]);

        DB::table("change_reason")->insert([
            "reason" => "Customer request",
        ]);

        DB::table("change_reason")->insert([
            "reason" => "Ticket",
        ]);

        DB::table("change_reason")->insert([
            "reason" => "Feature",
        ]);

        DB::table("change_reason")->insert([
            "reason" => "Other",
        ]);

        DB::statement("ALTER TABLE change_reason AUTO_INCREMENT = 1000");

        Schema::create("change", function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->text("description");
            $table->unsignedBigInteger("reason_id");
            $table->unsignedBigInteger("author_id");
            $table->unsignedBigInteger("status_id");
        });

        Schema::table("change", function (Blueprint $table) {
            $table->foreign("author_id")->references("id")->on("users");
            $table->foreign("status_id")->references("id")->on("change_status");
            $table->foreign("reason_id")->references("id")->on("change_reason");
        });

        Schema::create("change_commit", function (Blueprint $table) {
           $table->unsignedBigInteger("change_id");
           $table->unsignedBigInteger("commit_id");
           $table->smallInteger("order");
           $table->primary(["change_id","commit_id"]);
        });

        Schema::create("commit", function (Blueprint $table) {
           $table->id();
           $table->string("title");
           $table->text("description");
           $table->unsignedBigInteger("author_id");
           $table->dateTime("start_date");
           $table->dateTime("end_date");
           $table->dateTime("commit_date");
           $table->boolean("locking")->default(true);
           $table->boolean("commited")->default(false);
        });

        Schema::table("change_commit", function (Blueprint $table) {
            $table->foreign("change_id")->references("id")->on("change");
            $table->foreign("commit_id")->references("id")->on("commit");
        });

        Schema::create("service_type", function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("parent_service_type")->nullable();
            $table->string("name");
            $table->text("description")->nullable();
            $table->foreign("parent_service_type")->references("id")->on("service_type");
        });

        DB::table("service_type")->insert([
            "id" => 1,
            "name" => "Application"
        ]);

        DB::table("service_type")->insert([
            "id" => 2,
            "parent_service_type" => 1,
            "name" => "E-Mail"
        ]);

        DB::table("service_type")->insert([
            "id" => 3,
            "parent_service_type" => 1,
            "name" => "Web"
        ]);

        Schema::create("service_version", function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->text("description");
            $table->unsignedBigInteger("author_id");
            $table->unsignedBigInteger("supersedes")->nullable();
            $table->unsignedBigInteger("superseded_by")->nullable();
            $table->char('commit_hash', 128)->nullable();
            $table->timestamps();
        });

        Schema::table("commit", function (Blueprint $table) {
            $table->foreign("author_id")->references("id")->on("users");
        });

        Schema::create("service", function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("service_type_id");
            $table->unsignedBigInteger("current_version_id");
            $table->unsignedBigInteger("proposed_version_id")->nullable();
            $table->unsignedBigInteger("organization_id")->nullable();
            $table->timestamps();
        });

        Schema::table("service_version", function (Blueprint $table) {
            $table->foreign("supersedes")->references("id")->on("service_version");
            $table->foreign("superseded_by")->references("id")->on("service_version");
            $table->foreign("author_id")->references("id")->on("users");
        });

        Schema::table("service", function (Blueprint $table) {
            $table->foreign("current_version_id")->references("id")->on("service_version");
            $table->foreign("proposed_version_id")->references("id")->on("service_version");
            $table->foreign("service_type_id")->references("id")->on("service_type");
            $table->foreign("organization_id")->references("id")->on("organization");
        });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger("active_change")->nullable();
            $table->foreign("active_change")->references("id")->on("change");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
