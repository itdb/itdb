<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table("address")->insert([
            "id"=>0,
            "country"=>"Austria",
            "state"=>"Vienna",
            "district"=>"1010",
            "street"=>"Heldenplatz",
            "comment"=>"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam"
        ]);

        DB::table("organization")->insert([
            "id" => 0,
            "name" => "Demo Organization",
            "address_id"=>0
        ]);

        DB::table("organization")->insert([
            "id" => 1,
            "name" => "Demo Organization 2"
        ]);

        DB::table("service_version")->insert([
            "id"=>0,
            "name"=>"Demo Service",
            "description"=>"Service description",
            "author_id"=>1,
        ]);

        DB::table("service")->insert([
            "id"=>0,
            "service_type_id"=>1,
            "current_version_id"=>0,
            "organization_id"=>0
        ]);

        DB::table("service_version")->insert([
            "id"=>1,
            "name"=>"My Service 2",
            "description"=>"Service description",
            "author_id"=>1,
        ]);

        DB::table("service")->insert([
            "id"=>1,
            "service_type_id"=>1,
            "current_version_id"=>1,
            "organization_id"=>1
        ]);


    }
}
